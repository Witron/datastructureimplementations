package mx.edu.utr.datastructures.st1485;

import mx.edu.utr.datastructures.List;

/**
 *
 * @author Emmanuel Alejandro Alonso Guitron
 */
public class LinkedList implements List {

    
    //Variables used to create a cell
    int size;
    Node head;
    Node tail;

    //Set the centinels cells
    public LinkedList() {
        this.head = new Node(null, null, null);
        this.tail = new Node(null, this.head, null);
        this.head.next = this.tail;
    }

    //To create Nods 
    class Node {

        Object element;
        Node next;
        Node previous;

        //Constructor of Node
        private Node(Object element, Node next, Node previous) {
            this.element = element;
            this.next = next;
            this.previous = previous;
        }
    }

    /**
     * Increase the size of the linkedlist to add another cell
     * @param o Specifie the conteiner for that new cell
     * @param n set the node index 
     */
    public void addBefore(Object o, Node n) {

        size++;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(Object element) {
        addBefore(element, tail);
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(int index, Object element) {
        addBefore(element, getNode(index));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object set(int index, Object element) {

        Node n = getNode(index);
        Object old = n.element;
        n.element = element;
        return old;
    }

    /**
     * 
     * Obtainthe value in the index specified
     * @param index specify a position
     */
    private Node getNode(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("Out Of Bound");
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object get(int index) {
        return getNode(index).element;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int indexOf(Object o) {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object remove(int index) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return size();
    }
}
