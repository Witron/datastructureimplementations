package mx.edu.utr.datastructures.st1485;

import mx.edu.utr.datastructures.*;
/**
 *
 * @author Emmanuel Alejandro Alonso Guitron
 */
public class ArrayList implements List {

    public Object[] elements;
    public int size;

    public ArrayList(int initialCapacity) {
        elements = new Object[initialCapacity];
    }

    public ArrayList() {
        this(10);
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean add(Object element) {
        //ensureCapacity(size + 1);
        elements[size++] = element;
        return true;
    }

    private void ensureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        int newCapacity;
        if (minCapacity > oldCapacity) {
            newCapacity = oldCapacity * 2;

            Object arrayList[] = new Object[newCapacity];
            for (int i = 0; i < oldCapacity; i++) {
                arrayList[i] = elements[i];
            }
            elements = arrayList;
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(elements[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void clear() {
        for (int i = 0; i < elements.length; i++) {
            elements[i] = null;
            this.size = 0;
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void add(int index, Object element) {
        ensureCapacity(size + 1);
        for (int i = size; i >= index; i--) {
            elements[i + 1] = elements[i];
        }
        elements[index] = element;
        size++;
    }

    /**
     * Verify if the index tiped is correct or not
     * @param index Specify a position 
     */
    private void outOfBound(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("Out Of Bound");
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Object get(int index) {

        if (index >= this.size) {
            throw new IndexOutOfBoundsException("Out Of Bound");
        } else {
            for (int i = 0; i <= this.size; i++) {
                if (index == i - 1) {
                    elements[i] = index;
                }
            }
            return elements[index];
        }
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Object remove(int index) {
        outOfBound(index);
        Object oldElement = elements[index];

        int numberMoved = size - index - 1;
        if (numberMoved > 0) {
            System.arraycopy(elements, index + 1, elements, index, numberMoved);
        }
        elements[--size] = null;
        return oldElement;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Object set(int index, Object element) {
        Object old = elements[index];
        elements[index] = element;
        return old;
    }

    /**
     * Print the array to test it 
     */
    public void PrintArray() {
        for (int i = 0; i < elements.length; i++) {
            System.out.println(elements[i]);
        }
    }

}
